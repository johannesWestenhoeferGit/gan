# Generative Adversarial Networks

## Motivation

Um mit einem Lernalgorithmus $`\mathcal{A}`$ einen Parameter $`\hat{\theta} = \mathcal{A}(D_n)`$ mittels ERM zu finden s.d. $`R(\hat{\theta})`$ klein, wird eine große Datenmenge benötigt d.h. $`n \in \mathbb{N}`$ groß.

Doch für viele Problemstellungen ist es sehr schwierig bzw. kostspielig an eine entsprechende Menge gelabelte Daten zu kommen.

## Idee

Man stelle sich nun einen Fälscher und Gutachter vor.

Der Fälscher versucht Fälschungen herzustellen, die der Gutachter für echt hält. Am Anfang sind die Fälschungen sehr rudimentär und der Gutachter hat leichtes Spiel diese zu erkennen.

Doch mit der Zeit wird der Fälscher besser und auch der Gutachter muss neue Merkmale entdecken, an denen er die Fälschungen identifizieren kann.

Dieses Spiel setzt sich fort, bis entweder der Fälscher den Gutachter nicht mehr austricksen kann, oder die Fälschungen so gut geworden sind, dass der Gutachter diese nicht mehr erkennen kann.

## Mathematische Präzisierung

Es seien $`D_n = (X_j)_{1 \leq j \leq n} \subset \mathcal{X} = \mathbb{R}^d`$ mit $`n,d \in \mathbb{N}`$ die Daten.

### Generator

Sei $`\mathcal{U} = [0,1]^k`$,  mit $`k \in \mathbb{N}`$, $`U_i \sim Unif[0,1]`$ iid für $`i = 1,...,k`$ und  $`\mathcal{M}_\mathcal{G} = \{f_\theta : \theta \in \Theta_\mathcal{G}\}`$. Wir nennen $`U \in [0,1]^k`$ zufälliges Rauschen.


```math
 \mathcal{G}_\theta: \mathcal{U} \rightarrow \mathcal{X}, U \mapsto f_\theta(U) 
```


Wir nennen $`\mathcal{G}_\theta`$ den Generator und wir bezeichnen von $`\mathcal{G}`$ erzeugte Daten mit $`\hat{X}`$.

### Diskriminator

Sei $`\mathcal{Y} = [0,1]`$ und  $`\mathcal{M}_\mathcal{D} = \{f_\theta : \theta \in \Theta_\mathcal{D}\}`$


```math
 \mathcal{D}_\theta: \mathcal{X} \rightarrow \mathcal{Y}, X \mapsto f_\theta(X) 
```


Wir nennen $`\mathcal{D}_\theta`$ den Diskriminator.

### Adversarial Training

Beim Training eines Generative Adversarial Networks (GAN) trainieren wir einen Generator und einen Diskriminator "in Tandem" mit einer Abwandlung des MBGD Lernalgorithmus.

#### Verlustfunktionen

Wir definieren als Verlustfunktion für den Diskriminator $`\mathcal{D}`$ zu einem $`X \in \mathcal{X}`$ als die binäre Kreuzentropie:


```math
 L_\mathcal{D}(y,y') = H(y,y') \text{ mit } y =
    \begin{cases}
      1 & X \in D_n \\
      0 & \text{sonst}
    \end{cases}
    \text{ und } y' = \mathcal{D}(X)

```


und definieren die Verlustfunktion des Generators als


```math
 L_\mathcal{G}(x) = L_\mathcal{D}(1,\mathcal{D}(x)) = H(1,\mathcal{D}(x)) 
```


#### Lernalgorithmus

Analog zu Thema 14 definieren wir eine Abwandlung des MBGD Algorithmus als **Adversarial Training**.

Wähle Dimension des Rauschen $`k \in \mathbb{N}`$, Lernraten $`\eta_\mathcal{D}`$, $`\eta_\mathcal{G}`$, Startwerte $`\theta_\mathcal{D}^0`$, $`\theta_\mathcal{G}^0`$, eine Anzahl an Epochen $`N \in \mathbb{N}`$ und eine Batchsize $`B \in \mathbb{N}`$.

Der randomisierte Lernalgorithmus $`\mathcal{A}_\lambda`$ mit $`\lambda = (\eta_\mathcal{D},\eta_\mathcal{G},\theta_\mathcal{D}^0,\theta_\mathcal{G}^0,N,B,k)`$ funktioniert iterativ wie folgt:

 * Für $`e = 1 , ... , N`$:
   * Wähle eine zufällige gleichverteilte Permutation $`S = S^{(e)}`$ von $`\{1,...n\}`$ und bilde die Blöcke $`D_S^{(1)}`$, ..., $`D_S^{(\lfloor \frac{n}{B} \rfloor )}`$ mit $` D_S^{(i)} = (X_{S(iB+1)},...,X_{S(iB+B)}) \subset D_n`$
   * Für $`i = 1 , ... , \lfloor \frac{n}{B} \rfloor`$:
     * Erzeuge B zufälliges Rauschen $`U_1, ..., U_B \in [0,1]^k`$.
     * Berechne $` \hat{X}_1, ..., \hat{X}_B`$ = $`\mathcal{G}_{\theta^t}(U_1),...,\mathcal{G}_{\theta^t}(U_B) `$
     * Und update $` \theta `$ mit:
```math
\hat{L}_\mathcal{G} = \frac{1}{B}\underset{b=1}{\overset{B}{\sum}} L_\mathcal{D}(1,\mathcal{D}_{\theta_\mathcal{D}^t}(\hat{X}_b))
```
```math
\hat{L}_\mathcal{D} = \frac{1}{B}\underset{b=1}{\overset{B}{\sum}} L_\mathcal{D}(0,\mathcal{D}_{\theta_\mathcal{D}^t}(\hat{X}_b)) + \frac{1}{B}\underset{b=1}{\overset{B}{\sum}} L_\mathcal{D}(1,\mathcal{D}_{\theta_\mathcal{D}^t}(X_{S(iB+b)}))
```
```math
 \theta_\mathcal{G}^{t+1} = \theta_\mathcal{G}^{t} - \eta_\mathcal{G} \cdot \nabla_{\theta_\mathcal{G}} \hat{L}_\mathcal{G} 
```
```math
 \theta_\mathcal{D}^{t+1} = \theta_\mathcal{D}^{t} - \eta_\mathcal{D} \cdot \nabla_{\theta_\mathcal{D}} \hat{L}_\mathcal{D} 
```

 * $`\mathcal{A}_\lambda(D_n,V) = (\theta_\mathcal{G}^{(N \cdot \lfloor \frac{n}{B} \rfloor )}, \theta_\mathcal{D}^{(N \cdot \lfloor \frac{n}{B} \rfloor )})`$ mit Randomisierung $`V \sim Unif[0,1]`$


## Anwendungsbeispiel

### Beispiel

Mit dem Code in diesem Repository kann mit einem GAN Bilder die zum MNIST Datenset gehören könnten erzeugt werden.

Wir wählen $`N=100`$, $`B=128`$, $`k=256`$ und wählen MNIST Bilder als Daten.

Wir betrachten also:

```math
\mathcal{U} = [0,1]^{256}
```

```math
\mathcal{X} = \mathbb{R}^{28 \times 28} \cong \mathbb{R}^{784}
```

```math
\mathcal{Y} = [0,1]
```

**Vor dem Training:**

![](images/Epoch-0.png)

**Nach der 1. Epoche:**

![](images/Epoch-1.png)

**Nach der 10. Epoche:**

![](images/Epoch-10.png)

**Nach der 85. Epoche:**

![](images/Epoch-85.png)

### Anleitung

Die folgenden Beispielbilder wurden erzeugt mit:
```
python train.py -N 100 -B 128 -k 256 -S 4
N: Epochen
B: Batchsize
k: Dimension des Rauschen
S: Anzahl von Beispielbildern die nach jeder Epoche gespeichert werden.
```

Um alle benötigten Biblotheken zu installieren:
```
pip install -r requirements.txt
```