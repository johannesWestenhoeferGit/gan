def bar(i,n,length=30):
    filled = round(i/n*length)
    empty = length - filled
    return "[{}{}]".format("="*filled," "*empty)

def status(e,i,B,L_d,L_g,acc_t,acc_g):
    if e > 0 and i == 1: print("")
    print(
        "\r{:4d} | {:4d}/{:4d} | {} Generator-Loss: {:2.8f} | Discriminator-Loss: {:2.8f} | Acc-True: {:.3f} | Acc-Generated: {:.3f}".format(
            e, i, B, bar(i,B), L_g, L_d, acc_t, acc_g
        ),
        end = ""
    )