import tensorflow as tf

def generator(k):
    generator = tf.keras.Sequential()
    generator.add(tf.keras.layers.Dense(units=k, activation="relu", use_bias=False))
    generator.add(tf.keras.layers.BatchNormalization())
    generator.add(tf.keras.layers.Dense(units=k*2, activation="relu", use_bias=False))
    generator.add(tf.keras.layers.BatchNormalization())
    generator.add(tf.keras.layers.Dense(units=28*28, activation="relu", use_bias=False))
    generator.add(tf.keras.layers.Reshape((28,28)))
    return generator

def discriminator():
    discriminator = tf.keras.Sequential()
    discriminator.add(tf.keras.layers.Flatten())
    discriminator.add(tf.keras.layers.Dense(28*28,activation="relu"))
    discriminator.add(tf.keras.layers.Dense(1,activation="sigmoid"))

    return discriminator