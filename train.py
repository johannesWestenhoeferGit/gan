from argparse import ArgumentParser
import math

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

import models
from utils import status

# Lernalgorithmus Hyperparameter einlesen
parser = ArgumentParser()
parser.add_argument("-N","--epochs",dest="N")
parser.add_argument("-B","--batch-size",dest="B")
parser.add_argument("-k","--noise-dim",dest="k")
parser.add_argument("-S","--samples",dest="S")
args = parser.parse_args()
B = int(args.B)
N = int(args.N)
k = int(args.k)
samples = int(args.S)

# Daten laden und vorbereiten
mnist = tf.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0
x_train, x_test = np.reshape(x_train,(x_train.shape[0],x_train.shape[1],x_train.shape[2],1)), np.reshape(x_test,(x_test.shape[0],x_test.shape[1],x_test.shape[2],1))

# Modelraum festlegen
generator = models.generator(k)
discriminator = models.discriminator()

# Tensorflow optimizer festlegen
g_opt = tf.keras.optimizers.SGD()
d_opt = tf.keras.optimizers.SGD()

# Verlustfunktionen definieren
ce = tf.keras.losses.BinaryCrossentropy()

def loss_d(Y,Y_hat):
    L_bar = ce(tf.ones_like(Y),Y)
    L_hat = ce(tf.zeros_like(Y_hat),Y_hat)
    return L_hat + L_bar

def loss_g(Y_hat):
    return ce(tf.ones_like(Y_hat),Y_hat)

# Algorithmus wie in der README beschrieben
@tf.function
def step(X):
    # Erzeuge U zufälliges Rauschen
    U = tf.random.uniform(shape=(B,k))
    with tf.GradientTape() as g_tape, tf.GradientTape() as d_tape:
        # Berechne X_hat
        X_hat = generator(U,training=True)
        Y = discriminator(X,training=True)
        Y_hat = discriminator(X_hat, training=True)
        # Berechne L_D, L_G
        L_d = loss_d(Y,Y_hat)
        L_g = loss_g(Y_hat)

    # theta_G berechnen
    generator_gradients = g_tape.gradient(L_g,generator.trainable_variables)
    g_opt.apply_gradients(zip(generator_gradients,generator.trainable_variables))

    # theta_D berechnen
    discriminator_gradients = d_tape.gradient(L_d,discriminator.trainable_variables)
    d_opt.apply_gradients(zip(discriminator_gradients,discriminator.trainable_variables))

    return L_d, L_g, Y, Y_hat


def train(D_n):
    n = D_n.shape[0]
    # Erzeuge ein zufälliges aber statisches Rauschen mit dem wir den Fortschritt des Generators visualisieren können.
    U_static = tf.random.uniform(shape=(samples,k))
    for e in range(N):
        # Erzeuge Bilder mit dem Generator basierend auf statischem Rauschen.
        X_static = generator(U_static,training=False)

        # Plot leeren
        plt.clf();plt.cla()
        # Und für jedes Beispielbild
        for j, X_i in enumerate(X_static):
            # Das Bild darstellen
            plt.subplot(2,math.ceil(samples/2),j+1)
            plt.imshow(X_i)
        # Und als Datei speichern.
        plt.savefig("images/Epoch-{}.png".format(e))

        # Erzeuge Permutation S
        S = np.random.permutation(n)

        # Und die D_S^(i) Blöcke
        D = [ D_n[S[i*B:i*B+B]] for i in range(math.floor(n/B)) ]

        # Wir sammeln Loss & Genauigkeit für spätere Ausgabe
        E_L_d, E_L_g = [], []
        E_acc_t, E_acc_g = [], []
        
        # Für jeden Block D_i
        for i, D_i in enumerate(D):
            # Den Algorithmus ausführen
            L_d, L_g, Y, Y_hat = step(D_i)
            # Und Loss & Genauigkeit tracken.
            E_L_d.append(L_d), E_L_g.append(L_g)
            Y_arr, Y_hat_arr = Y.numpy(), Y_hat.numpy()
            Y_c, Y_hat_c = Y_arr[Y_arr > 0.5], Y_hat_arr[Y_hat_arr <= 0.5]
            acc_true, acc_generated = Y_c.shape[0]/Y_arr.shape[0], Y_hat_c.shape[0]/Y_hat_arr.shape[0]
            E_acc_t.append(acc_true), E_acc_g.append(acc_generated)
            # Und den Status darstellen
            status(e,i+1,len(D),np.average(E_L_d),np.average(E_L_g),np.average(E_acc_t), np.average(E_acc_g))

if __name__ == "__main__":        
    train(x_train)